# localstack test

run gitlab runner locally (fails):

```
wget https://gitlab-runner-downloads.s3.amazonaws.com/master/binaries/gitlab-runner-linux-amd64
reset && time ./gitlab-runner-linux-amd64 exec docker test
```

run plain (works):
```
env/bin/pip install pytest boto3
virtualenv env
dd if=/dev/zero of=1M count=1024 bs=1024
dd if=/dev/zero of=10M count=10024 bs=1024
dd if=/dev/zero of=100M count=100024 bs=1024
docker-compose up
env/bin/pytest -s test_s3_client.py
```
