import os.path
import boto3


class S3Client:
    def __init__(self):
        self.client = self._client()

    def list_folder_contents(self, bucket_name, folder_name=None, exclude_self=True):
        if folder_name:
            folder_name = os.path.join(folder_name, "")  # ensure it ends in a slash
        else:
            folder_name = ""  # non-prefixed -- all folders

        objects = []
        incomplete = True
        continuation_token = None

        while incomplete:
            if continuation_token:
                response = self.client.list_objects_v2(
                    Bucket=bucket_name,
                    Prefix=folder_name,
                    ContinuationToken=continuation_token,
                )
            else:
                response = self.client.list_objects_v2(
                    Bucket=bucket_name, Prefix=folder_name
                )

            objects += response.get("Contents", [])
            if response.get("isTruncated", False):
                continuation_token = response["NextContinuationToken"]
            else:
                incomplete = False

        if exclude_self:
            contents = [obj["Key"] for obj in objects if obj["Key"] != folder_name]
        else:
            contents = [obj["Key"] for obj in objects]

        return contents

    def upload_file(self, source_name, target_name, bucket_name):
        print("Upload: %s" % " ".join([source_name, target_name, bucket_name]))
        self.client.upload_file(source_name, bucket_name, target_name)

    def download_file(self, source_name, target_name, bucket_name):
        print("Download: %s" % " ".join([source_name, target_name, bucket_name]))
        self.client.download_file(bucket_name, source_name, target_name)

    def _client(self):
        s3_endpoint = os.getenv("S3_ENDPOINT")
        return boto3.client(
            "s3", aws_access_key_id="dummy", use_ssl=False, aws_secret_access_key="dummy",
            endpoint_url=s3_endpoint,
        )
