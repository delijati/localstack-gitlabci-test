import os.path
import unittest
from tempfile import TemporaryDirectory
from s3_client import S3Client

TEST_BUCKET = "testing-s3-bucket"


class TestS3Client(unittest.TestCase):
    def setUp(self):
        self.client = S3Client()
        self.client.client.create_bucket(Bucket=TEST_BUCKET)

    def tearDown(self):
        # self.client.client.delete_bucket(Bucket=TEST_BUCKET)
        self.client = None

    def test_up_down_file(self):
        RANGE = int(os.getenv("TEST_RANGE", "10"))
        for i in range(RANGE):
            target_file = "TEST/1M-%s" % i

            self.client.upload_file("1M", target_file, TEST_BUCKET)
            contents = self.client.list_folder_contents(TEST_BUCKET, "TEST")
            self.assertIn(target_file, contents)
            # self.client.client.delete_object(Bucket=TEST_BUCKET, Key=target_file)

        target_file = "TEST/10M"
        self.client.upload_file("10M", target_file, TEST_BUCKET)
        target_file = "TEST/100M"
        self.client.upload_file("100M", target_file, TEST_BUCKET)

        with TemporaryDirectory() as tdir:
            for i in range(RANGE):
                source = "TEST/1M-%s" % i
                target_file = os.path.join(tdir, "target-%s.dat" % i)
                self.client.download_file(source, target_file, TEST_BUCKET)
                self.assertTrue(os.path.exists(target_file))

            source = "TEST/10M"
            target_file = os.path.join(tdir, "target-10M.dat")
            self.client.download_file(source, target_file, TEST_BUCKET)
            source = "TEST/100M"
            target_file = os.path.join(tdir, "target-100M.dat")
            self.client.download_file(source, target_file, TEST_BUCKET)
